import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.charset.StandardCharsets;
import java.io.FileWriter;

public class GitRevert {

    // Branch list 
    private static List<String> branchs = new ArrayList<>();

    // Directories
    private static String homeDir = "/home/mazzariol/git";
    private static String jHawkDir = homeDir + "/ic/JHawkAcademic/CommandLine/";
    private static String sourceDir = homeDir + "/";
    private static String destinationDir = homeDir + "/";

    // JHawk Command
    private static String jHawkCLI = "java -jar " + jHawkDir + "JHawkCommandLine.jar ";
    private static String properties = "-p " + jHawkDir + "jhawkbase.properties ";
    private static String filter = "-f .*\\.java -r -l pcm -s ";
    private static String source = sourceDir + "junit5";

    // Commands
    private static String cmdHeadGitLog = "git log -n 1";

    private static void runCommand(String cmd, String directory){
        String sInput = null;
        String sError = null;
        try{
            System.out.println("Command:\t" + cmd + "\nSource:\t" + directory);
            Process p = Runtime.getRuntime().exec(cmd, null, new File(directory));
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            while ((sInput = stdInput.readLine()) != null) {
                System.out.println(sInput);
            }
            while ((sError = stdError.readLine()) != null) {
                System.out.println(sError);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void getBranchs(long number){
        String sInput = null;
        String sError = null;
        String cmdGetBranhs = "";
        try{
            cmdGetBranhs = "git log --branches -" + number + " --pretty=format:%H";
            Process p = Runtime.getRuntime().exec(cmdGetBranhs, null, new File(source));

            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            while ((sInput = stdInput.readLine()) != null) {
                branchs.add(sInput);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void restoreEnvironment(String directory){
        try {
            runCommand("git checkout master", directory);
            runCommand("git pull", directory);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void extractMetricsFromFile(String fileName){
        try {
            String outputFileName = fileName.replace("reports", "metrics").replace(".csv", "_metrics.csv");
            File inputFile = new File(fileName);
            FileWriter outputFile = new FileWriter(outputFileName);
            List<String> inputLines = Files.readAllLines(inputFile.toPath(), StandardCharsets.UTF_8);
            for (String line : inputLines) { 
               String[] array = line.split("&");
               outputFile.append(array[0]).append("&").append(array[1]).append("&").append(array[2])
               .append("&").append(array[3]).append("&").append(array[4]).append("&").append(array[8])
               .append("&").append(array[10]).append("&").append(array[12]).append("&").append(array[13])
               .append("&").append(array[18]).append("&").append(array[25]).append("&").append(array[30])
               .append("&").append(array[31]).append("&").append(array[33]).append("&").append(array[35])
               .append("&").append(array[37]).append("\n");
            }
            outputFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void refactorCSV(long n){
        String fileName = "";
        int i = 1;
        while( i <= n) {
            fileName = "/home/mazzariol/git/ic/reports/report" + i + ".csv";
            extractMetricsFromFile(fileName);
            i++;
        }
    }

    public static void main(String[] args) {        

        long n = Long.parseLong(args[0]);
        int i = 0;
        String cmdRollBack = "git reset HEAD~" + 1;
        String destination = "";
        String cmdGenReport = "";

        getBranchs(n);
        while (i < branchs.size()) {
            System.out.println("\n****************************************************************************\n");
            cmdRollBack = "git checkout " + branchs.get(i);
            runCommand(cmdRollBack, source);
            runCommand(cmdHeadGitLog, source);
            System.out.println("\n****************************************************************************\n");

            destination = " -raw c -c " + destinationDir + "ic/reports/report" + (i+1) +".csv -v &";
            cmdGenReport = jHawkCLI + properties + filter + source + destination;
        
            runCommand(cmdGenReport, source);
            i++;
    	}
        restoreEnvironment(source);

        refactorCSV(n);
    }
}