import abstracts.Report;
import com.google.gson.JsonObject;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;

class Analysis {

    private final static Logger LOGGER = LogManager.getLogger(Analysis.class);

    static void start(Report report) {
        if (report.getType().isJHawk()) {
            jHawkAnalysis((JHawkReport) report);
        }
    }

    private static void jHawkAnalysis(JHawkReport report) {
        LOGGER.info("Start JHawkAcademic analysis...");
        try {
            if (report.getjHawkDir() == null || !Files.isDirectory(Paths.get(report.getjHawkDir()))){
                LOGGER.error("JHawk directory '" + report.getjHawkDir() + "' does not exists.");
            } else if (report.getProjectDir() == null || !Files.isDirectory(Paths.get(report.getProjectDir()))){
                LOGGER.error("Project directory '" + report.getProjectDir() + "' does not exists.");
            } else {

                if (report.createEnvironmentDirs()){
                    long createStartTime = System.currentTimeMillis();
                    report.createReport();
                    long createStopTime = System.currentTimeMillis();
                    long extractStartTime = System.currentTimeMillis();
                    report.extractMetrics();
                    long extractStopTime = System.currentTimeMillis();
                    long jsontStartTime = System.currentTimeMillis();
                    JsonObject graphicData = report.getJSONMetrics();
                    long jsonStopTime = System.currentTimeMillis();
                    long jsonStartRefactor = System.currentTimeMillis();
                    JsonObject refactorGraphicData = report.removeConstantsMetric(graphicData);
                    long jsonStopRefactor = System.currentTimeMillis();
                    FileWriter fileWriter = new FileWriter(report.getFinalJsonName());
                    fileWriter.write("var data = " + refactorGraphicData.toString());
                    fileWriter.close();
                    NumberFormat formatter = new DecimalFormat("#0.00000");
                    LOGGER.info("* CREATE REPORT TIME: " + formatter.format((createStopTime - createStartTime) / 1000d) + " seconds");
                    LOGGER.info("* EXTRACT METRICS REPORT TIME: " + formatter.format((extractStopTime - extractStartTime) / 1000d) + " seconds");
                    LOGGER.info("* JSON GRAPHIC DATA TIME: " + formatter.format((jsonStopTime - jsontStartTime) / 1000d) + " seconds");
                    LOGGER.info("* JSON REFACTOR DATA TIME: " + formatter.format((jsonStopRefactor - jsonStartRefactor) / 1000d) + " seconds");
                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
        LOGGER.info("Finished JHawkAcademic analysis...");
    }
}
