import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

class CSV {

    // TODO metrics with param
    static boolean extractMetricsFromFile(String inputFile, String outputFile) {
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputFile));
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                    .withDelimiter('&')
                    .withHeader("Package", "Name", "No. Methods", "LCOM", "HEFF", "INST", "RFC", "CBO", "F-IN", "LCOM2",
                            "FOUT", "EXT", "TCC", "MPC", "COH", "HVOL", "HLTH")); //"HDIF", "HVOC"

            Reader reader = Files.newBufferedReader(Paths.get(inputFile));
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                    .withDelimiter('&')
                    .withFirstRecordAsHeader()
                    .withIgnoreHeaderCase()
                    .withTrim());

            for (CSVRecord csvRecord : csvParser) {
                csvPrinter.printRecord(csvRecord.get("Package"), csvRecord.get("Name"),
                        csvRecord.get("No. Methods"), csvRecord.get("LCOM"), csvRecord.get("HEFF"),
                        csvRecord.get("INST"), csvRecord.get("RFC"), csvRecord.get("CBO"), csvRecord.get("F-IN"),
                        csvRecord.get("LCOM2"), csvRecord.get("FOUT"), csvRecord.get("EXT"), csvRecord.get("TCC"),
                        csvRecord.get("MPC"), csvRecord.get("COH"), csvRecord.get("HVOL"), csvRecord.get("HLTH"));
//                        csvRecord.get("HDIF"), csvRecord.get("HVOC")
            }
            csvPrinter.flush();
            csvPrinter.close();
            csvParser.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    static List<CSVRecord> getCSVLines(String fileName) throws IOException {
        Reader reader = Files.newBufferedReader(Paths.get(fileName));
        return new CSVParser(reader, CSVFormat.DEFAULT
                .withDelimiter('&')
                .withFirstRecordAsHeader()
                .withIgnoreHeaderCase()
                .withTrim()).getRecords();
    }
}
