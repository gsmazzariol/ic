import interfaces.Command;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

class CommandGit implements Command {

    private static Logger LOGGER = Logger.getLogger(CommandGit.class);
    private List<String> branchs = new ArrayList<String>();

    public CommandGit() {
        super();
    }

    public JSONObject run(String cmd, String directory) {
        JSONObject jsonObject = new JSONObject();
        List<String> output = new ArrayList<String>();
        List<String> outputError = new ArrayList<String>();
        String sInput, sError;
        try {
            LOGGER.info("Source: " + directory + " - git command: " + cmd);
            Process p = Runtime.getRuntime().exec(cmd, null, new File(directory));
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            output = new ArrayList<String>();
            while ((sInput = stdInput.readLine()) != null) {
                LOGGER.info(sInput);
                output.add(sInput);
            }

            outputError = new ArrayList<String>();
            while ((sError = stdError.readLine()) != null) {
                LOGGER.info(sError);
                outputError.add(sError);
            }
        } catch (Exception e) {
            LOGGER.error("CommandGit - Failed to run command", e);
        } finally {
            jsonObject.put("sInput", output);
            jsonObject.put("sError", outputError);
        }
        return jsonObject;
    }

    /*
    * Get a number of git commit's hashs to a specific source.
    * */
    void getCommitHash(long number, String source) {
        String cmdGetBranhs = "git log -" + number + " --pretty=format:%H_%ct";
        JSONObject output = run(cmdGetBranhs, source);

        JSONArray jsonArray = output.getJSONArray("sInput");
        for (int j =0 ; j < jsonArray.length(); j++){
            branchs.add(String.valueOf(jsonArray.get(j)));
        }
        LOGGER.info(branchs.toString());
    }

    /*
     * Restore a git environment to a specific source.
     * */
    void restoreEnvironment(String directory) {
        run("git checkout master", directory);
        run("git pull", directory);
    }

    public List<String> getCommits() {
        return branchs;
    }
}
