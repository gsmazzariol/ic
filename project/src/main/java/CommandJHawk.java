import interfaces.Command;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

class CommandJHawk implements Command {

    private static Logger LOGGER = Logger.getLogger(CommandJHawk.class);

    public CommandJHawk() {
        super();
    }

    public JSONObject run(String cmd, String directory) {
        JSONObject jsonObject = new JSONObject();
        List<String> output = new ArrayList<String>();
        List<String> outputError = new ArrayList<String>();
        String sInput, sError;
        try {
            LOGGER.info("Source: " + directory + " - JHawk command: " + cmd);
            Process p = Runtime.getRuntime().exec(cmd, null, new File(directory));
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            output = new ArrayList<String>();
            while ((sInput = stdInput.readLine()) != null) {
                LOGGER.info(sInput);
                output.add(sInput);
            }

            outputError = new ArrayList<String>();
            while ((sError = stdError.readLine()) != null) {
                LOGGER.info(sError);
                outputError.add(sError);
            }
        } catch (Exception e) {
            LOGGER.error("CommandJHawk - Failed to run command", e);
        } finally {
            jsonObject.put("sInput", output);
            jsonObject.put("sError", outputError);
        }
        return jsonObject;
    }
}
