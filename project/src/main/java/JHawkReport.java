import abstracts.Report;
import com.google.gson.JsonObject;
import enums.ReportEnum;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JHawkReport extends Report {

    private final static Logger LOGGER = LogManager.getLogger(JHawkReport.class);

    private String versions;
    private String jHawkDir;
    private String projectDir;
    private String projectName;
    private String destDir;
    private boolean minimun;
    private List<String> reportFiles;
    private HashMap<String, CSVRecord> csvRecords;
    private List<String> metrics;

    private List<String> reportFilesWithMetrics;

    JHawkReport(String jHawkDir, String projectDir, String versions, String destDir, String minimun) {
        this.jHawkDir = jHawkDir;
        this.projectDir = projectDir;
        this.versions = versions;
        this.destDir = destDir;
        this.minimun = Boolean.valueOf(minimun);
        this.setType(ReportEnum.JHAWK);
        this.setCommand("java -jar " + jHawkDir + "/CommandLine/JHawkCommandLine.jar -p "
                + jHawkDir + "/CommandLine/jhawkbase.properties "
                + "-f .*\\.java"
                + " -r -l pcm -s "
                + projectDir + " -raw c -c");
        this.reportFiles = new ArrayList<String>();
        this.csvRecords = new HashMap<String, CSVRecord>();
        this.reportFilesWithMetrics = new ArrayList<String>();
        String[] aux = projectDir.split("/");
        this.projectName = aux[aux.length-1];
        this.metrics = Arrays.asList("LCOM", "HEFF", "INST", "RFC", "CBO", "F-IN", "LCOM2", "FOUT", "EXT", "TCC", "MPC", "COH", "HVOL", "HLTH");
    }

    String getjHawkDir() {
        return jHawkDir;
    }

    String getProjectDir() {
        return projectDir;
    }

    public String getVersions() {
        return versions;
    }

    public String getDestDir() {
        return destDir;
    }

    public List<String> getReportFilesWithMetrics() {
        return reportFilesWithMetrics;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public boolean isMinimun() {
        return minimun;
    }

    public String getFinalJsonName(){
        String finalJson = "";
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDate localDate = LocalDate.now();
        String todayDate = dtf.format(localDate).replace("/", "_");
        finalJson = String.format("%s/json/%s_%s.json", this.destDir, this.projectName, todayDate);
        return finalJson;
    }

    boolean createEnvironmentDirs() {
        boolean s1 = true, s2 = true, s3 = true, s4 = true;
        try {
            Path reportDir = Paths.get(this.destDir + "/reports");
            Path metricDir = Paths.get(this.destDir + "/metrics");
            Path resultsDir = Paths.get(this.destDir + "/results");
            Path jsonDir = Paths.get(this.destDir + "/json");

            if (!Files.isDirectory(reportDir) && !reportDir.equals(Files.createDirectories(reportDir))) {
                LOGGER.error("Failed to create directory: " + reportDir.toAbsolutePath().toString());
                s1 = false;
            }
            if (!Files.isDirectory(metricDir) && !metricDir.equals(Files.createDirectories(metricDir))) {
                LOGGER.error("Failed to create directory: " + metricDir.toAbsolutePath().toString());
                s2 = false;
            }
            if (!Files.isDirectory(resultsDir) && !resultsDir.equals(Files.createDirectories(resultsDir))) {
                LOGGER.error("Failed to create directory: " + resultsDir.toAbsolutePath().toString());
                s3 = false;
            }
            if (!Files.isDirectory(jsonDir) && !jsonDir.equals(Files.createDirectories(jsonDir))) {
                LOGGER.error("Failed to create directory: " + jsonDir.toAbsolutePath().toString());
                s4 = false;
            }
        } catch (Exception e) {
            LOGGER.error("Failed to create environment directories. {}", e);
        }
        return s1 && s2 && s3 && s4;
    }

    @Override
    public void createReport() {

        CommandGit gitCmd = new CommandGit();
        CommandJHawk jHawk = new CommandJHawk();

        gitCmd.getCommitHash(Long.valueOf(this.versions), this.projectDir);
        for (String commit : gitCmd.getCommits()) {
            String reportFile = this.destDir + "/reports/report_" + commit + ".csv";
            reportFiles.add(reportFile);
            if (!minimun && !Files.exists(Paths.get(reportFile))){
                String cmdRollBack = "git checkout " + commit.split("_")[0];
                gitCmd.run(cmdRollBack, this.projectDir);
                String jHawkPlus = this.getCommand() + " " + reportFile + " -v &";
                jHawk.run(jHawkPlus, this.projectDir);
            }
        }
        if (!minimun){
            gitCmd.restoreEnvironment(this.projectDir);
        }
    }

    void extractMetrics() {
        try {
            for (String file : reportFiles) {
                String outputFile = file.replace("report", "metric");
                reportFilesWithMetrics.add(outputFile);
                if (!minimun && !Files.exists(Paths.get(outputFile))) {
                    CSV.extractMetricsFromFile(file, outputFile);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    JsonObject getJSONMetrics() {
        try {
            JsonObject packageList = new JsonObject();
            for (String metricFile : reportFilesWithMetrics) {
                Matcher commitMatcher = Pattern.compile("[a-f0-9]{40}").matcher(metricFile);
                Matcher dataMatcher = Pattern.compile("[0-9]{10}.csv").matcher(metricFile);
                String commitDate = (dataMatcher.find() ? dataMatcher.group() : "").replace(".csv", "") + "000";
                String commitHash = (commitMatcher.find() ? commitMatcher.group() : "");
                List<CSVRecord> csvLines = CSV.getCSVLines(metricFile);
                for (CSVRecord record: csvLines) {
                    if (!packageList.has(record.get("Package"))){
                        packageList.add(record.get("Package"), new JsonObject());
                    }
                    JsonObject pkg = (JsonObject) packageList.get(record.get("Package"));
                    if (!pkg.has(record.get("Name"))){
                        pkg.add(record.get("Name"), new JsonObject());
                    }
                    JsonObject className = (JsonObject) pkg.get(record.get("Name"));
                    if (!className.has(commitDate)){
                        className.add(commitDate, new JsonObject());
                    }
                    JsonObject hash = (JsonObject) className.get(commitDate);
                    hash.addProperty("commit", commitHash);
                    hash.addProperty("No. Methods", record.get("No. Methods"));
                    this.metrics.forEach(metric -> {
                        hash.addProperty(metric, record.get(metric));
                    });
                }
            }
            return packageList;
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return null;
    }

    JsonObject removeConstantsMetric(JsonObject data) {
        HashMap<String, List<String>> keys = new HashMap<>();
        List<String> toDelete = new ArrayList<>();
        for (String pkg : data.keySet()) {
            JsonObject classList = (JsonObject) data.get(pkg);
            for (String cls : classList.keySet()){
                // List metrics in period
                keys.clear();
                this.metrics.forEach(metric -> {
                    keys.put(metric, new ArrayList<>());
                });
                JsonObject commitList = (JsonObject) classList.get(cls);
                for (String commit : commitList.keySet()){
                    this.metrics.forEach(metric -> {
                        keys.get(metric).add(String.valueOf(((JsonObject) commitList.get(commit)).get(metric)));
                    });
                }
                // Check which metrics are constant
                this.metrics.forEach(metric -> {
                    if (new HashSet<String>(keys.get(metric)).size() <= 1){
                        toDelete.add(metric);
                    }
                });

                // Remove constants metrics
                for (String commit : commitList.keySet()){
                    toDelete.forEach(metric -> {
                        ((JsonObject) commitList.get(commit)).remove(metric);
                    });
                }
                // Remove Empty CommitList
                toDelete.clear();
                for (String commit : commitList.keySet()){
                    if (((JsonObject) commitList.get(commit)).size() == 2)
                        toDelete.add(commit);
                }
                toDelete.forEach(commitList::remove);
            }
            // Remove Empty ClassList
            toDelete.clear();
            for (String clsOnPackage : classList.keySet()){
                if (((JsonObject)classList.get(clsOnPackage)).size() == 0){
                    toDelete.add(clsOnPackage);
                }
            }
            toDelete.forEach(classList::remove);
        }
        // Remove Empty Packages
        toDelete.clear();
        for (String pkg : data.keySet()) {
            if (((JsonObject) data.get(pkg)).size() == 0){
                toDelete.add(pkg);
            }
        }
        toDelete.forEach(data::remove);
        return data;
    }
}
