import abstracts.Report;
import enums.Opt;
import org.apache.commons.cli.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Main {

    private final static Logger LOGGER = LogManager.getLogger(Main.class);

    private static Options constructOptions() {
        Options options = new Options();
        options.addOption(Opt.RP.getValue(), Opt.RP.getArgs(), Opt.RP.getDescription());
        options.addOption(Opt.JH.getValue(), Opt.JH.getArgs(), Opt.JH.getDescription());
        options.addOption(Opt.PD.getValue(), Opt.PD.getArgs(), Opt.PD.getDescription());
        options.addOption(Opt.DD.getValue(), Opt.DD.getArgs(), Opt.DD.getDescription());
        options.addOption(Opt.PV.getValue(), Opt.PV.getArgs(), Opt.PV.getDescription());
        options.addOption(Opt.VR.getValue(), Opt.VR.getArgs(), Opt.VR.getDescription());
        options.addOption(Opt.HP.getValue(), Opt.HP.getArgs(), Opt.HP.getDescription());
        options.addOption(Opt.MN.getValue(), Opt.MN.getArgs(), Opt.MN.getDescription());
        return options;
    }

    private static void parseOptions(String[] args, Options options) {
        String header = "This program does an analysis of Java programs, at levels of coupling and cohesion.\n\n";
        String footer = "\nPlease report issues at g138466@dac.unicamp.br";
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);

            if (cmd.getOptions().length == 0 || cmd.hasOption(Opt.HP.getValue())) {
                formatter.printHelp("JavaAnalysisCode", header, options, footer, true);
            } else if (cmd.hasOption(Opt.VR.getValue())) {
                LOGGER.info("Version 1.0.1");
            } else if (!cmd.hasOption(Opt.RP.getValue())) {
                LOGGER.error("Report type was not found.");
                formatter.printHelp("JavaAnalysisCode", header, options, footer, true);
            } else if (cmd.hasOption(Opt.RP.getValue())) {

                if (cmd.getOptionValue(Opt.RP.getValue()).equals("jHawk")) {
                    Report report = new JHawkReport(
                            cmd.getOptionValue(Opt.JH.getValue()), cmd.getOptionValue(Opt.PD.getValue()),
                            cmd.getOptionValue(Opt.PV.getValue()), cmd.getOptionValue(Opt.DD.getValue()),
                            cmd.getOptionValue(Opt.MN.getValue()));
                    Analysis.start(report);
                } else {
                    LOGGER.error("Report type '" + cmd.getOptionValue(Opt.RP.getValue())
                            + "' was not recognized as a valid report type");
                    formatter.printHelp("JavaAnalysisCode", header, options, footer, true);
                }
            }

        } catch (UnrecognizedOptionException e) {
            LOGGER.error(e.getMessage());
            formatter.printHelp("JavaAnalysisCode", header, options, footer, true);
        } catch (ParseException parseOptions) {
            LOGGER.error("Failed to parse options", parseOptions);
        }
    }

    public static void main(String[] args) {
        //Log4j Basic configuration
        BasicConfigurator.configure();

        // create the Opt
        Options options = constructOptions();

        // parse options
        parseOptions(args, options);
    }
}
