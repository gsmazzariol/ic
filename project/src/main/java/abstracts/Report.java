package abstracts;

import enums.ReportEnum;

public abstract class Report {

    private String command;
    private ReportEnum type;

    public abstract void createReport();

    public ReportEnum getType() {
        return type;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public void setType(ReportEnum type) {
        this.type = type;
    }

    public String getTypeName() {
        return type.getType();
    }

    public String getCommand() {
        return command;
    }
}
