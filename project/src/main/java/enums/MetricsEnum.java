package enums;

public enum MetricsEnum {

    NMETHODS("No. Methods", "/home/mazzariol/git/ic/results/Methods.csv"),
    LCOM("LCOM", "/home/mazzariol/git/ic/results/LCOM.csv"),
    HEFF("HEFF", "/home/mazzariol/git/ic/results/HEFF.csv"),
    INST("INST", "/home/mazzariol/git/ic/results/INST.csv"),
    RFC("RFC", "/home/mazzariol/git/ic/results/RFC.csv"),
    CBO("CBO", "/home/mazzariol/git/ic/results/CBO.csv"),
    FIN("F-IN", "/home/mazzariol/git/ic/results/FIN.csv"),
    LCOM2("LCOM2", "/home/mazzariol/git/ic/results/LCOM2.csv"),
    FOUNT("FOUT", "/home/mazzariol/git/ic/results/FOUT.csv"),
    EXT("EXT", "/home/mazzariol/git/ic/results/EXT.csv"),
    TCC("TCC", "/home/mazzariol/git/ic/results/TCC.csv"),
    MPC("MPC", "/home/mazzariol/git/ic/results/MPC.csv"),
    COH("COH", "/home/mazzariol/git/ic/results/COH.csv"),
    HVOL("HVOL", "/home/mazzariol/git/ic/results/HVOL.csv"),
//    HDIF("HDIF", "/home/mazzariol/git/ic/results/HDIF.csv"),
//    HVOC("HVOC", "/home/mazzariol/git/ic/results/HVOC.csv"),
    HLTH("HLTH", "/home/mazzariol/git/ic/results/HLTH.csv");

    private String value;
    private String fileName;

    //todo get root path
    MetricsEnum(String value, String fileName) {
        this.value = value;
        this.fileName = fileName;
    }

    public String getValue() {
        return value;
    }

    public String getFileName() {
        return fileName;
    }
}
