package enums;

public enum Opt {

    RP("rP", "report-type", true, "type of report. The types are: 'jHawk'"),
    JH("jH", "jhawk-dir", true, "JHawkAcademic directory."),
    PD("pD", "project-dir", true, "directory of the project to be analyzed."),
    PV("pV", "project-versions", true, "the number of versions to be analyzed."),
    DD("dD", "destination-dir", true, "destination directory to store results."),
    VR("v", "version", false, "Print the version of the application."),
    HP("h", "help", false, "options usage."),
    MN("mN", "minumun", true, "show only metrics that are not constants in the period");

    private String alias;
    private String option;
    private Boolean args;
    private String description;

    Opt(String alias, String option, Boolean args, String desctiption) {
        this.alias = alias;
        this.option = option;
        this.args = args;
        this.description = desctiption;
    }

    public String getValue() {
        return alias;
    }

    public String getOption() {
        return option;
    }

    public Boolean getArgs() {
        return args;
    }

    public String getDescription() {
        return description;
    }

    public boolean isReporType(String s){
        return s.equals(RP.getValue()) || s.equals(RP.getOption());
    }

    public boolean isJhawkDir(String s){
        return s.equals(JH.getValue()) || s.equals(JH.getOption());
    }

    public boolean isProjectDir(String s){
        return s.equals(PD.getValue()) || s.equals(PD.getOption());
    }

    public boolean isDestDir(String s){
        return s.equals(DD.getValue()) || s.equals(DD.getOption());
    }

    public boolean isProjectVersion(String s){
        return s.equals(PV.getValue()) || s.equals(PV.getOption());
    }

    public boolean isMinimum(String s){
        return s.equals(MN.getValue()) || s.equals(MN.getOption());
    }

    public boolean isVersion(String s){
        return s.equals(VR.getValue()) || s.equals(VR.getOption());
    }

    public boolean isHelp(String s){
        return s.equals(HP.getValue()) || s.equals(HP.getOption());
    }
}
