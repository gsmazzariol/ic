package enums;

public enum ReportEnum {

    JHAWK("JHawk");

    private String type;

    ReportEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public boolean isJHawk(){
        return this.getType().equals(JHAWK.getType());
    }
}
