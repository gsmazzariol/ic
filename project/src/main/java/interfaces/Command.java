package interfaces;

import org.json.JSONObject;

public interface Command {

    JSONObject run(String cmd, String directory);
}
